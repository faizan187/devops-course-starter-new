# DevOps Apprenticeship: Project Exercise

> If you are using GitPod for the project exercise (i.e. you cannot use your local machine) then you'll want to launch a VM using the [following link](https://gitpod.io/#https://github.com/CorndelWithSoftwire/DevOps-Course-Starter). Note this VM comes pre-setup with Python & Poetry pre-installed.

## System Requirements

The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.7+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

### Poetry installation (Bash)

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -
```

### Poetry installation (PowerShell)

```powershell
(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py -UseBasicParsing).Content | python -
```

## Dependencies

The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:

```bash
$ poetry install
```

ls

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

```bash
$ cp .env.template .env  # (first time only)
```

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change). There's also a [SECRET_KEY](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY) variable which is used to encrypt the flask session cookie.

## Running the App

Once the all dependencies have been installed, start the Flask app in development mode within the Poetry environment by running:
```bash
$ poetry run flask run
```

You should see output similar to the following:
```bash
 * Serving Flask app "app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 226-556-590
```
Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.



The following enviroment variables will need to be created for this app to funstion

## Change the following values for local development.

### Flask server configuration.
```bash
 * FLASK_APP=todo_app/app
 * FLASK_ENV=development
```

### Change the following values for local development.
```bash
 * SECRET_KEY=<>
 * TRELLO_KEY=<>
 * TRELLO_TOKEN=<>
 * TRELLO_BOARD_ID=<>
 * TRELLO_ID_LIST_TODO=<>
 * TRELLO_ID_LIST_DONE=<>
```


## Testing the APP

You can run both unit and integration tests suites using pytest. Run this from the root directory:

poetry run pytest

Or you can run them from VSCode:

Click the conical flask icon on the activity bar on the left edge of VSCode. Click the refresh icon at the top of the panel to rediscover tests. Click the play icon at the top to run all tests. Click the play icon next to a file or test name to run that file or test individually.

Intellisense annotations for running/debugging each test should also appear above the test functions in the code.
If test discovery fails, check that "poetry install" ran successfully and that the Python interpreter is selected correctly. See the "setup" section above for details.


## Docker Container

The Dockerfile is a multistage Dockerfile which has been broken down into three phases
1- Generic build e.g. updates and packages that will form the base for both prod and dev containers
2- Instructions for a PROD container
3- Instruction for a Dev Container

Build:

Use the following commands to build the images, the second image should take less time as it reuses most of the layers from the generic build

docker build --target production --tag todoapp:prod .
docker build --target development --tag todoapp:dev .


Run the container using the command below

Production Container:
The PROD container can be run using the command below

docker run --detach --env-file ./.env --publish 80:8000 todoapp:prod

The production container is using Gunicorn to run the application instead of Flask and has FLASK_ENV set to production.

Developmant Container:
The Dev container is run using the command below

docker run --detach --env-file ./.env --publish 5000:5000 todoapp:dev

Use a mount bind so all changes made in the host directory are reflected in the docker container with out rerunning the build, please use

docker run --detach --env-file ./.env --publish 5000:5000 --mount type=bind,source="$(pwd)"/todo_app,target=/opt/todoapp/todo_app todo-app:dev

Verify the mount bind using the command 

docker inspect <container name of ID>, you should see a similer output

"Mounts": [
    {
        "Type": "bind",
        "Source": <PATH_TO_TODO_APP>",
        "Destination": "/opt/todoapp/todo_app",
        "Mode": "",
        "RW": true,
        "Propagation": "rprivate"

Try creating a new file in your host directory and it should be automatically replicated in your docker file

Docker Compose:

Once you have created your docker-compose.yml file, to build all images and run the containers, use

docker compose up (use the -d switch if you want to run the containers in the backgroud)

