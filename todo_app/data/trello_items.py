import os
import requests
from todo_app.data.item import Item

def get_items_trello():
    trello_key = os.getenv('TRELLO_KEY')
    trello_token = os.getenv('TRELLO_TOKEN')
    trello_board_id = os.getenv('TRELLO_BOARD_ID')
    trello_lists = requests.get(f"https://api.trello.com/1/boards/{trello_board_id}/lists?key={trello_key}&token={trello_token}&cards=open").json()
    items = []
    for list in trello_lists:
        for card in list['cards']:
            item = Item.from_trello_card(card, list)
            items.append(item)
    return items


def add_item_trello(title):
    trello_key = os.getenv('TRELLO_KEY')
    trello_token = os.getenv('TRELLO_TOKEN')
    trello_list_id_todo = os.getenv('TRELLO_ID_LIST_TODO')
    trello_response = requests.post(f"https://api.trello.com/1/cards?key={trello_key}&token={trello_token}&name={title}&pos=bottom&idList={trello_list_id_todo}")


def move_item_to_doing(card_id):
    trello_key = os.getenv('TRELLO_KEY')
    trello_token = os.getenv('TRELLO_TOKEN')
    trello_list_id_doing = os.getenv('TRELLO_ID_LIST_DOING')
    trello_move_to_doing = requests.put(f"https://api.trello.com/1/cards/{card_id}?key={trello_key}&token={trello_token}&idList={trello_list_id_doing}")


def move_item_to_done(card_id):
    trello_key = os.getenv('TRELLO_KEY')
    trello_token = os.getenv('TRELLO_TOKEN')
    trello_list_id_done = os.getenv('TRELLO_ID_LIST_DONE')
    trello_move_to_done = requests.put(f"https://api.trello.com/1/cards/{card_id}?key={trello_key}&token={trello_token}&idList={trello_list_id_done}")


def move_item_to_todo(card_id):
    trello_key = os.getenv('TRELLO_KEY')
    trello_token = os.getenv('TRELLO_TOKEN')
    trello_list_id_todo = os.getenv('TRELLO_ID_LIST_TODO')
    trello_move_to_done = requests.put(f"https://api.trello.com/1/cards/{card_id}?key={trello_key}&token={trello_token}&idList={trello_list_id_todo}")


def delete_card(card_id):
    trello_key = os.getenv('TRELLO_KEY')
    trello_token = os.getenv('TRELLO_TOKEN')
    trello_move_to_done = requests.delete(f"https://api.trello.com/1/cards/{card_id}?key={trello_key}&token={trello_token}")