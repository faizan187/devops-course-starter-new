class ViewModel:
    def __init__(self, items):
        self._items = items

    @property
    def items(self):
        return self._items

    @property
    def todo_items(self):
        todo_card_status = False
        todo_items = []
        for item in self._items:
            if item.status == "to do":
                todo_items.append(item)
                todo_card_status = True
        return todo_items, todo_card_status

    @property
    def doing_items(self):
        doing_card_status = False
        doing_items = []
        for item in self._items:
            if item.status == "doing":
                doing_items.append(item)
                doing_card_status = True
        return doing_items, doing_card_status

    @property
    def done_items(self):
        done_card_status = False
        done_items = []
        for item in self._items:
            if item.status == "done":
                done_items.append(item)
                done_card_status = True
        return done_items, done_card_status
