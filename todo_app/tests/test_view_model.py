"""Unit tests for todo app"""

import pytest
from todo_app.data.view_model import ViewModel
from todo_app.data.item import Item

@pytest.fixture
def viewmodel() -> ViewModel:
    items = []
    todo_item_1 = Item(1, 'todo item 1', 'to do')
    todo_item_2 = Item(2, 'todo item 2', 'to do')
    items.append(todo_item_1)
    items.append(todo_item_2)
    doing_item_1 = Item(1, 'doing item 1', 'doing')
    doing_item_2 = Item(2, 'doing item 2', 'doing')
    items.append(doing_item_1)
    items.append(doing_item_2)
    done_item_1 = Item(1, 'done item 1', 'done')
    done_item_2 = Item(2, 'done item 2', 'done')
    items.append(done_item_1)
    items.append(done_item_2)
    return ViewModel(items)


def test_all_items(viewmodel: ViewModel):
    assert len(viewmodel.items) == 6


def test_doing_items_count(viewmodel: ViewModel):
    assert len(viewmodel.doing_items[0]) == 2

def test_doing_items_status(viewmodel: ViewModel):
    assert viewmodel.doing_items[1] == True


def test_todo_items_count(viewmodel: ViewModel):
    assert len(viewmodel.todo_items[0]) == 2

def test_todo_items_status(viewmodel: ViewModel):
    assert viewmodel.todo_items[1] == True


def test_done_items_count(viewmodel: ViewModel):
    assert len(viewmodel.done_items[0]) == 2

def test_done_items_status(viewmodel: ViewModel):
    assert viewmodel.done_items[1] == True
    




