from ast import iter_child_nodes
from flask import Flask, render_template, redirect, request
from todo_app.flask_config import Config
from todo_app.data.trello_items import add_item_trello, move_item_to_done, move_item_to_todo, delete_card, get_items_trello, move_item_to_doing
from todo_app.data.view_model import ViewModel


def create_app():
    app = Flask(__name__)
    app.config.from_object(Config())


    @app.route('/')
    def index():
        items = get_items_trello()
        item_view_model = ViewModel(items)
        return render_template('index.html', view_model = item_view_model)


    @app.route('/add', methods=['POST'])
    def add():
        title = add_item_trello(request.form.get('title'))
        return redirect('/')


    @app.route('/start_item/<id>')
    def start_item(id):
        move_item_to_doing(id)
        return redirect('/')


    @app.route('/complete_item/<id>')
    def complete_item(id):
        move_item_to_done(id)
        return redirect('/')


    @app.route('/incomplete_item/<id>')
    def incomplete_item(id):
        move_item_to_todo(id)
        return redirect('/')


    @app.route('/delete_item/<id>')
    def delete_item(id):
        delete_card(id)
        return redirect('/')

    return app