FROM python:3.7-slim as base

ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_VERSION=1.3.1 \
    POETRY_HOME="/opt/poetry" \
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    POETRY_NO_INTERACTION=1 \
    APP_PATH="/opt/todoapp" \
    VENV_PATH="/opt/todoapp/.venv" \
    FLASK_APP=todo_app/app 

# # prepend poetry and venv to path
ENV PATH="$POETRY_HOME/bin:$VENV_PATH/bin:$PATH"

RUN apt-get update
RUN apt-get install -y curl
WORKDIR /opt/todoapp
RUN curl -sSL https://install.python-poetry.org | POETRY_HOME=$POETRY_HOME python3 -
COPY poetry.lock pyproject.toml ./
RUN poetry install

COPY todo_app/ ./todo_app/

FROM base as production

ENV FLASK_ENV=production


EXPOSE 8000
ENTRYPOINT [ "poetry", "run" ]
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "todo_app.app:create_app()"]


FROM base as development

ENV FLASK_ENV=development

EXPOSE 5000
ENTRYPOINT [ "poetry", "run" ]
CMD ["flask", "run", "--host=0.0.0.0"]


FROM base as test

ENTRYPOINT [ "poetry", "run", "ptw", "--poll"]